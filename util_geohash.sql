create or replace PACKAGE util_geohash 
AS
	type coord is record(lat number(12,9), lon number(12,9));
	type rect is record(ne coord, sw coord);
	
	TYPE map_pair IS RECORD(dir varchar2(12), geohash varchar2(12));
	TYPE map_pair_table IS TABLE OF map_pair;

	precision_min int := 1;
	precision_max int := 12;

	latMin number(12,9) :=  -90;
	latMax number(12,9) :=   90;
	lonMin number(12,9) := -180;
	lonMax number(12,9) :=  180;
	
	function to_str(p_coord coord) return varchar2 deterministic;

	function encode(p_lat number, p_lon number, p_precision int default null) return varchar2 deterministic;
	function encode(p_coord coord, p_precision int default null) return varchar2 deterministic;
	function decode(p_geohash varchar2) return coord deterministic;
	
	function bounds(p_geohash varchar2 default '') return rect deterministic;

END UTIL_GEOHASH;

/
create or replace PACKAGE BODY UTIL_GEOHASH AS

	-- (geohash-specific) Base32 map
	c_base32 varchar2(100) := '0123456789bcdefghjkmnpqrstuvwxyz'; 

	function to_str(p_coord coord) 
		return varchar2 
		deterministic
	as
	begin
		return '['||p_coord.lat||','||p_coord.lon||']';
	end;


	function encode(p_coord coord, p_precision int default null) 
		return varchar2
		deterministic
	as 
	begin
--		if(p_coord is null)then
--			return null;
--		end if;
		return encode(p_coord.lat, p_coord.lon, p_precision);
	end;

	/**
	 * Encodes latitude/longitude to geohash, either to specified precision or to automatically
	 * evaluated precision.
	 *
	 * @param   {number} lat - Latitude in degrees.
	 * @param   {number} lon - Longitude in degrees.
	 * @param   {number} [precision] - Number of characters in resulting geohash.
	 * @returns {string} Geohash of supplied latitude/longitude.
	 * @throws  Invalid geohash.
	 *
	 * @example
	 *     geohash varchar2 := util_geohash.encode(52.205, 0.119, 7); -- => 'u120fxw'
	 */
	function encode(p_lat number, p_lon number, p_precision int default null) 
		return varchar2
		deterministic
	as
		v_precision int := p_precision;
		v_lat number(12,9) := p_lat;
		v_lon number(12,9) := p_lon;

		v_idx int := 0; -- index into base32 map
		v_bit int := 0; -- each char holds 5 bits
		v_evenBit boolean := true;
		v_geohash varchar2(12) := ''; -- 11 digits is accurate to 1.5x1.5cm
		v_posn coord := null;

		v_latMid number(12,9);
		v_lonMid number(12,9);
		v_latMin number(12,9) := latMin;
		v_lonMin number(12,9) := lonMin;
		v_latMax number(12,9) := latMax;
		v_lonMax number(12,9) := lonMax;

	begin
		DBMS_OUTPUT.PUT_LINE('enc: Start'); 
		DBMS_OUTPUT.PUT_LINE('enc: LAT ['||v_lat||']'); 
		DBMS_OUTPUT.PUT_LINE('enc: LON ['||v_lon||']'); 
		DBMS_OUTPUT.PUT_LINE('enc: PRECISION ['||v_precision||']'); 
		if(p_lat is null or p_lat is null)then
			return null;
		end if;
		
		-- infer precision?
		if (v_precision is null) then
			v_precision := precision_max; -- set to maximum
			-- refine geohash until it matches precision of supplied lat/lon
			for p in precision_min..precision_max loop
				v_geohash := encode(v_lat, v_lon, p);
				v_posn := decode(v_geohash);
				if (v_posn.lat = v_lat and v_posn.lon = v_lon) then 
					return v_geohash;
				end if;
			end loop;
		end if;

		DBMS_OUTPUT.PUT_LINE('enc: geohash ['||v_geohash||']'); 
		while (length(v_geohash) < v_precision or v_geohash is null) loop
			if (v_evenBit) then
				DBMS_OUTPUT.PUT_LINE('enc: bisect E-W longitude');
				v_lonMid := (v_lonMin + v_lonMax) / 2;
				DBMS_OUTPUT.PUT_LINE('enc: -['||v_lonMin||','||v_lonMax||'] --> '||v_lon||'>='||v_lonMid); 
				if (v_lon >= v_lonMid) then
					v_idx := v_idx*2 + 1;
					v_lonMin := v_lonMid;
				else
					v_idx := v_idx*2;
					v_lonMax := v_lonMid;
				end if;
				DBMS_OUTPUT.PUT_LINE('enc: ['||v_lonMin||','||v_lonMax||'] <-- '||v_lon||','||v_idx); 
			else
				DBMS_OUTPUT.PUT_LINE('enc: bisect N-S latitude');
				DBMS_OUTPUT.PUT_LINE('enc: |['||v_latMin||','||v_latMax||'] --> '||v_lat||'>='||v_latMid); 
				v_latMid := (v_latMin + v_latMax) / 2;
				if (v_lat >= v_latMid) then
					v_idx := v_idx*2 + 1;
					v_latMin := v_latMid;
				else
					v_idx := v_idx*2;
					v_latMax := v_latMid;
				end if;
			end if;
			v_evenBit := not v_evenBit;

			v_bit := v_bit + 1;
			DBMS_OUTPUT.PUT_LINE('enc: v_bit ['||v_bit||']'); 
			if (v_bit = 5) then
				-- 5 bits gives us a character: append it and start over
				v_geohash := v_geohash || substr(c_base32, v_idx+1, 1);
				v_bit := 0;
				v_idx := 0;
			end if;
			DBMS_OUTPUT.PUT_LINE('enc: geohash ['||v_geohash||']'); 
		end loop;

		DBMS_OUTPUT.PUT_LINE('enc: Done'); 
		return v_geohash;
	end encode;


	/**
	 * Decode geohash to latitude/longitude 

	 * location is approximate centre of geohash cell, to reasonable precision
	 *
	 * @param   {string} geohash - Geohash string to be converted to latitude/longitude.
	 * @returns {{lat:number, lon:number}} (Center of) geohashed location.
	 * @throws  Invalid geohash.
	 *
	 * @example
	 *     const latlon := Geohash.decode('u120fxw'); -- => { lat: 52.205, lon: 0.1188 }
	 */
	function decode(p_geohash varchar2)
		return coord
		deterministic
	as
		v_rtn coord;
		v_bounds rect := null;

		v_lat number(12,9) := null;
		v_lon number(12,9) := null;
		v_digits float := null;
		
		v_latMin number(12,9) := null;
		v_lonMin number(12,9) := null;
		v_latMax number(12,9) := null;
		v_lonMax number(12,9) := null;
	begin
		DBMS_OUTPUT.PUT_LINE('dec: Start'); 
		v_bounds := bounds(p_geohash); -- <-- the hard work
		-- now just determine the centre of the cell...

		v_latMin := v_bounds.sw.lat;
		v_lonMin := v_bounds.sw.lon;
		v_latMax := v_bounds.ne.lat;
		v_lonMax := v_bounds.ne.lon;
		DBMS_OUTPUT.PUT_LINE('dec: lat ['||v_latmin||'-->'||v_latmax||']'); 
		DBMS_OUTPUT.PUT_LINE('dec: lon ['||v_lonmin||'-->'||v_lonmax||']'); 

		-- cell centre
		v_lat := (v_latMin + v_latMax)/2;
		v_lon := (v_lonMin + v_lonMax)/2;
		DBMS_OUTPUT.PUT_LINE('dec: +Center ['||v_lat||','||v_lon||']'); 

		-- round to close to centre without excessive precision: ⌊2-log10(Δ°)⌋ decimal places
		v_digits := power(10, floor(2-log(10,v_latMax-v_latMin)));
		DBMS_OUTPUT.PUT_LINE('dec: -precision ['||v_digits||']'); 
		v_lat := round(v_lat*v_digits)/v_digits;
		DBMS_OUTPUT.PUT_LINE('dec: -lat ['||v_lat||']'); 
		v_digits := power(10, floor(2-log(10,v_lonMax-v_lonMin)));
		DBMS_OUTPUT.PUT_LINE('dec: |precision ['||v_digits||']'); 
		v_lon := round(v_lon*v_digits)/v_digits;
		DBMS_OUTPUT.PUT_LINE('dec: |lon ['||v_lon||']'); 

		v_rtn.lat := v_lat;
		v_rtn.lon := v_lon;
		DBMS_OUTPUT.PUT_LINE('dec: Done '||util_geohash.to_str(v_rtn)); 

		return v_rtn;
	end;


	/**
	 * Returns SW/NE latitude/longitude bounds of specified geohash.
	 *
	 * @param   {string} geohash - Cell that bounds are required of.
	 * @returns {{sw: {lat: number, lon: number}, ne: {lat: number, lon: number}}}
	 * @throws  Invalid geohash.
	 */
	function bounds(p_geohash varchar2 default '')
		return rect
		deterministic
	as
		v_rtn rect;
		v_geohash varchar2(12) := lower(p_geohash);

		isEvenBit boolean := true;
		v_latMin number(12,9) :=  -90; 
		v_latMax number(12,9) :=   90;
		v_lonMin number(12,9) := -180; 
		v_lonMax number(12,9) :=  180;

		v_latMid number(12,9);
		v_lonMid number(12,9);

		v_char char(1);
		v_idx  int;
		v_bitN int;
		
		function bitshift(val number, amt number) return number
		as
			v_rtn number(9,0) := val;
			v_exp number(9,0) := amt;
		begin
			v_rtn := floor(v_rtn * power(2, v_exp));
			return v_rtn;
		end;
		

	begin
		DBMS_OUTPUT.PUT_LINE('bnd: start ['||v_geohash||']'); 
		if (v_geohash is null) then
			RAISE_APPLICATION_ERROR(-20001, 'Invalid geohash');
		end if;


		for i in 1..length(v_geohash) loop
			v_char := substr(v_geohash, i, 1);
			v_idx := instr(c_base32, v_char)-1;
			DBMS_OUTPUT.PUT_LINE('bnd: '||i||': '||v_char); 
			DBMS_OUTPUT.PUT_LINE('bnd: '||i||': '||v_idx); 

			if (v_idx is null) then
				RAISE_APPLICATION_ERROR(-21002, 'Invalid geohash');
			end if;

			for n in reverse 0..4 loop
				v_bitN := bitshift(v_idx, -1*n);
				v_bitN := bitand(v_bitN, 1);
				if (isEvenBit) then
					-- longitude
					v_lonMid := (v_lonMin+v_lonMax) / 2;
					if (v_bitN = 1) then
						v_lonMin := v_lonMid;
					else
						v_lonMax := v_lonMid;
					end if;
				else
					-- latitude
					v_latMid := (v_latMin+v_latMax) / 2;
					if (v_bitN = 1) then
						v_latMin := v_latMid;
					else
						v_latMax := v_latMid;
					end if;
				end if;
				isEvenBit := not isEvenBit;
			end loop;
		end loop;

		v_rtn.sw.lat := v_latMin;
		v_rtn.sw.lon := v_lonMin;
		v_rtn.ne.lat := v_latMax;
		v_rtn.ne.lon := v_lonMax;
		DBMS_OUTPUT.PUT_LINE('bnd: done '||to_str(v_rtn.sw)||to_str(v_rtn.ne)); 
		return v_rtn;
	end;


	/**
	 * Determines adjacent cell in given direction.
	 *
	 * @param   geohash - Cell to which adjacent cell is required.
	 * @param   direction - Direction from geohash (N/S/E/W).
	 * @returns {string} Geocode of adjacent cell.
	 * @throws  Invalid geohash.
	 */
	function adjacent(geohash varchar2, direction varchar2) 
		return varchar2
		deterministic
	as
		v_rtn varchar2(12) := null;
		v_geohash varchar2(12) := lower(geohash);
		v_direction varchar2(2) := lower(direction);

		v_type int;
		v_parent varchar2(12);
		v_child varchar2(1);
		v_lastch varchar2(1);

		function neighbour(dir char, latlon int) return varchar2 deterministic
		as 
		begin
			return case lower(dir)
				when 'n' then case when latlon = 0 then 'p0r21436x8zb9dcf5h7kjnmqesgutwvy' else 'bc01fg45238967deuvhjyznpkmstqrwx' end
				when 's' then case when latlon = 0 then '14365h7k9dcfesgujnmqp0r2twvyx8zb' else '238967debc01fg45kmstqrwxuvhjyznp' end
				when 'e' then case when latlon = 0 then 'bc01fg45238967deuvhjyznpkmstqrwx' else 'p0r21436x8zb9dcf5h7kjnmqesgutwvy' end
				when 'w' then case when latlon = 0 then '238967debc01fg45kmstqrwxuvhjyznp' else '14365h7k9dcfesgujnmqp0r2twvyx8zb' end
				else null
			end;
		end;

		function border(dir char, latlon int) 
			return varchar2 deterministic
		as
			v_border varchar2(10) := null;
		begin
			v_border := case lower(dir)
					when 'n' then case when latlon = 0 then 'prxz'     else 'bcfguvyz' end
					when 's' then case when latlon = 0 then '028b'     else '0145hjnp' end
					when 'e' then case when latlon = 0 then 'bcfguvyz' else 'prxz'     end
					when 'w' then case when latlon = 0 then '0145hjnp' else '028b'     end
				end;
			return v_border;
		end;
		
	begin

		if (v_geohash is null) then
			RAISE_APPLICATION_ERROR(-21001, 'Invalid geohash');
		end if;
		if (v_direction not in ('n','s','e','w')) then
			RAISE_APPLICATION_ERROR(-21003, 'Invalid direction');
		end if;

		v_child  := substr(v_geohash,length(v_geohash)-1,1); -- last character of hash
		v_parent := substr(v_geohash,1,length(v_geohash)-1); -- hash without last character

		v_type := mod(length(geohash), 2);

		-- check for edge-cases which don't share common prefix
		if (instr(border(v_direction,v_type), v_lastCh) != -1 and v_parent != '') then
			v_parent := adjacent(v_parent, direction);
		end if;

		-- append letter for direction to parent
		v_rtn := v_parent;
		if(v_rtn is null)then
			v_rtn := neighbour(v_direction,v_type);
			v_rtn := instr(v_rtn, v_lastCh);
			v_rtn := substr(c_base32, v_rtn);
		end if;
		return v_rtn;
	end;


	/**
	 * Returns all 8 adjacent cells to specified geohash.
	 *
	 * @param   {string} geohash - Geohash neighbours are required of.
	 * @returns {{n,ne,e,se,s,sw,w,nw: string}}
	 * @throws  Invalid geohash.
	 */
	function neighbours(geohash varchar2)
		return map_pair_table
		deterministic
	as 
		v_recs map_pair_table := map_pair_table();
	begin
		v_recs.extend(8);
		
		v_recs(1).dir := 'n' ; v_recs(1).geohash := adjacent(geohash, 'n');
		v_recs(2).dir := 'e' ; v_recs(2).geohash := adjacent(geohash, 'e');
		v_recs(3).dir := 's' ; v_recs(3).geohash := adjacent(geohash, 's');
		v_recs(4).dir := 'w' ; v_recs(4).geohash := adjacent(geohash, 'w');
		v_recs(5).dir := 'ne'; v_recs(5).geohash := adjacent(v_recs(1).geohash, 'e');
		v_recs(6).dir := 'se'; v_recs(6).geohash := adjacent(v_recs(3).geohash, 'e');
		v_recs(7).dir := 'sw'; v_recs(7).geohash := adjacent(v_recs(3).geohash, 'w');
		v_recs(8).dir := 'nw'; v_recs(8).geohash := adjacent(v_recs(1).geohash, 'w');
		
		return v_recs;
	end;


END UTIL_GEOHASH;
