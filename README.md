# Geohash for OracleDB

A PLSQL port of the Geohash encoding system.

https://www.movable-type.co.uk/scripts/geohash.html
Geohash: Gustavo Niemeyer’s geocoding system.


A Geohash management utility.


| strlen  | lat bits | lng bits | lat error  | lng error | error (km)   | scale |
| :---    | -------: | -------: | ---------- | --------- | :-------     | ----: |
| 1       |        2 |        3 | ±23        | ±23       |  ±2,500      |  km   |
| 2       |        5 |        5 | ± 2.8      | ± 5.6     |    ±630      |  km   |
| 3       |        7 |        8 | ± 0.70     | ± 0.70    |     ±78      |  km   |
| 4       |       10 |       10 | ± 0.087    | ± 0.18    |     ±20      |  km   |
| 5       |       12 |       13 | ± 0.022    | ± 0.022   |      ±2.4    |  km   |
| 6       |       15 |       15 | ± 0.0027   | ± 0.0055  |      ±0.61   |   m   |
| 7       |       17 |       18 | ± 0.00068  | ± 0.00068 |      ±0.076  |   m   |
| 8       |       20 |       20 | ± 0.000085 | ± 0.00017 |      ±0.019  |   m   |
| 9       |          |          |            |           |              |   m   |
| 10      |          |          |            |           |              |   m   |
| 11      |          |          |            |           |              |  cm   |
| 12      |          |          |            |           |              |  cm   |

see 
- [Digits and precision in km](https://en.wikipedia.org/wiki/Geohash)
- [ALL YOU NEED TO KNOW ABOUT GEOHASH](https://docs.quadrant.io/quadrant-geohash-algorithm)
- [Moveable Type](https://www.movable-type.co.uk/scripts/geohash.html)


# Usage

## Convert between GeoHash and Lat/Lon

We can find the Library of Parliament using this process, except with absurd accuracy.

```sql
select 
	util_geohash.encode(45.425, -75.700233) as "CAN Parliament",
	util_geohash.to_str(util_geohash.decode('f244mmkyb')) as "CAN Parliament (coord center)"
from 
	dual;
```

## Changing Accuracy

```sql
select 
	util_geohash.bounds('f244mmkyb') as "Window into Parliament (coord)"
	util_geohash.bounds('f244mmk') as "Parliament"
from 
	dual;
```


## Find Nearby

Given a location, find nearby communities

``` sql

```

## General Queries

```sql
select 
	util_geohash.decode('sunny') as "Saudi Arabia",
	util_geohash.decode('fur') as "Greenland",
	util_geohash.decode('reef') as "Coral Sea",
	util_geohash.decode('geek') as Iceland,
	util_geohash.decode('queen') as "Priscilla, Queen of the Desert"
from
	dual
;
```

## Basic Test Run

```sql
declare
	v_hash varchar2(1000);
begin
	--v_hash := util_geohash.encode(45.425, -75.700233);
	--v_hash := util_geohash.decode('f244mmkyb');
	
	v_hash := 'f244mmkyb';
	if(v_hash <> util_geohash.encode(util_geohash.decode(v_hash),length(v_hash)))then
		raise_application_error(-20000, 'decode+encode did not result in same value');
	end if;
end;
```

# License

Copyright 2014 - Jeff Cave

[MIT License](./LICENSE)

